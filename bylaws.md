# DSA San Francisco Bylaws

###### Ratified April 26th, 2017 / Amended March 8th, 2023

## Table of Contents

<!-- Auto Generated, Do not remove -->

# Article I. Name

The name of this organization shall be the Democratic Socialists of America, San Francisco (“DSA SF”), a local chapter (“Chapter”) of the Democratic Socialists of America (“DSA”). The name is taken from the city and county of San Francisco, California (“San Francisco”) that the Chapter will serve.

# Article II. Purpose

## Section 1. Vision

DSA SF’s vision is a United States that is liberated from the control and corruption of capitalism in our economy, society, and community. Instead of a political-economic system which benefits an elite few, we envision a social order based on democratic control of resources and production, economic planning, equitable distribution, and social and economic justice for all. We envision San Francisco as an exemplar of these values.

## Section 2. Mission

DSA SF’s mission is to organize the people of San Francisco in order to establish and exercise collective power that can fight capitalism, combat inequality, and bring about real socialist change.

## Section 3. Objectives

1.  Build collective power that can stand up to capital.
2.  Actively organize throughout San Francisco to exhibit solidarity, build stronger communities, and bring about real material change for the people.
3.  Educate ourselves and the people of San Francisco on socialist critiques of capitalism and advocate for democratic socialist values, vision, and policy.
4.  Act as a San Francisco hub for DSA National events, projects, and campaigns.
5.  Partner with, contribute to, and support adjacent DSA Chapters in the San Francisco Bay Area, California.

# Article III. Membership

## Section 1. Qualifications

Membership shall be open to dues-paying members of DSA who reside in or hold employment in San Francisco or the larger San Francisco Bay Area and are not members of another DSA Chapter.

## Section 2. Dues

Annual dues are set by the DSA National and must be paid to the DSA National office. Voluntary member dues may be established on behalf of DSA SF, but their nonpayment may not restrict membership status or eligibility to serve on any committees or working groups.

## Section 3. “Good Standing”

Members are considered in “good standing” provided that they are considered in good standing with DSA National.

## Section 4. “Active”

Members are considered “active” provided they have attended two of the three most recent “Regular Meetings,” consistently participate in Working Groups or Committees, as testified by nomination by a chair of a Working Group or Committee they are in, or if they are working towards a DSA SF priority as put forward by the Steering Committee member responsible for said priority.

## Section 5. “Eligible Voter”

Members are considered “eligible voters” provided they are both active and in good standing as per Article III Sections 3 and 4.

## Section 6: DSA SF Member Expectations

There is an expectation for all members to participate in creating a safe and open space, so that our chapter can assert our priorities. In doing this there is an expectation that all members follow the code of conduct and are respectful to all other members and potential members, in all spaces, including: in person, slack, discord, the forum, signal, and email. In addition, in order for the chapter to effectively run there is an expectation that membership will participate in supporting the infrastructure and/or priorities.

## Section 7. Suspension and Expulsion

The Steering Committee may expel or suspend a member by a two-thirds vote provided the member is found to be in substantial disagreement with the principles or policies of the organization; they consistently engage in undemocratic, disruptive behavior; they publicly misrepresent the formal endorsements or positions adopted by the Chapter pursuant to [Article XII](#article-xii-elections); or they are under the discipline of any self-defined democratic-centralist organization. Members facing expulsion or suspension must receive written notice of the charges against them and must be given a reasonable opportunity to provide a written response to the Steering Committee (which must be in addition to any opportunity they may have to provide a written response to the Grievance Officers) prior to the aforementioned vote. After charges to suspend or expel a member are discharged or dismissed, the Steering Committee shall report those charges and the results of the process to the membership as soon as reasonably possible. Reports shall be made in summary form to protect the privacy and identities of the involved individual(s), and shall identify how Steering Committee members voted and which were recused.

In addition, pending the outcome of an investigation, grievance, or disciplinary process, the Steering Committee by majority vote may temporarily suspend a member’s access to any chapter communication channels, social media, events, meetings, or other spaces if necessary to protect the safety of individuals or the chapter.

Any member of the Steering Committee who cannot reasonably be expected to fairly adjudicate an investigation, grievance, or disciplinary process (such as if they are the complainant or if their own behavior is at issue) shall automatically be recused. All voting thresholds shall be based on the number of non-recused Steering Committee members.

## Section 8. Sexual Harassment and Anti-Discrimination Policy

Sexual harassment and discrimination on the basis of such inappropriate grounds as age, disability, national origin, race, religion, gender or sexual identity or orientation are antithetical to the principles of democratic socialism and to the vision and mission of DSA SF. A member determined to have engaged in sexual harassment or inappropriate discrimination shall be deemed to be in substantial disagreement with the principles or policies of DSA SF and may be considered for suspension or expulsion under [Article III Section 7](#section-7-suspension-and-expulsion).

DSA SF may adopt a formal policy that defines sexual harassment and inappropriate discrimination for purposes of this section and/or prescribes procedures for resolving allegations of sexual harassment or inappropriate discrimination.

# Article IV. Steering Committee

## Section 1. Members

The Officers of DSA SF shall compose the Steering Committee.

## Section 2. Responsibilities

-   The Steering Committee shall meet as a whole at least once per quarter (in person or by conference call). Those meetings shall be open to chapter members unless the Committee goes into executive session by two-thirds vote of the committee.
-   The Steering Committee shall concern itself primarily with establishing program activities and proposing guidelines for consideration by the general membership.
-   The Steering Committee shall be responsible for providing the organizational infrastructure required for DSA SF’s meetings and for members to accomplish projects and tasks within working groups and committees.
-   The Steering Committee, working with relevant committees, must publish annual reports describing the planned projects and accomplishments of DSA SF to members two weeks prior to the Annual Convention.
-   The Steering Committee is responsible for organizing efforts to satisfy DSA’s priorities within San Francisco, as determined by the general membership.
-   The Steering Committee shall be responsible for presenting budgetary appropriations to the members at Regular Meetings, to be approved by a majority vote of members present.
-   The Steering Committee shall be responsible for acting on the organization's behalf between Regular Meetings and the Annual Convention.

Upon election of a new Steering Committee, the current Steering Committee will stay on in an Emeritus form for one month in order to facilitate the transfer of leadership. They will assist the new Steering Committee in any business which requires continuity, and provide feedback on their established Plan of Work. This will include:
-   Providing an overview of the Bylaws, code of conduct, and grievance process to incoming members;
-   Providing training for the Treasurer as to taxes, expenditures, and managing the chapter’s bank account;
-   Coordinating with CCC to familiarize incoming members with the division of labor between the Steering Committee and CCC;
-   Flagging any open/ongoing issues that were brought to the outgoing Steering Committee and having availability to answer questions related to those issues
-   Providing an overview and guidance regarding the Steering Committee’s portfolios as mentioned in [Article IV Section 3](#section-3-portfolios) of the Bylaws.

## Section 3. Portfolios.

In order to distribute work within the Steering Committee and provide active guidance to the chapter, the Steering Committee shall establish “portfolios” of work that Committee members will drive forward during their term. These portfolios may consist of a set of goals for improving chapter infrastructure, plans for engagement with chapter Committees, Working Groups, and priority campaigns toward shared ends, or other divisions of work within the Steering Committee.

Portfolios will consist of a description of the plan for the given year, and contact information for the Steering Committee member responsible for that portfolio.

Together, these portfolios will comprise a Plan of Work for the Steering Committee in their term. This Plan of Work shall be distributed to all chapter members within one month of taking office, and the Steering Committee shall report at least semesterly on the progress of this Plan with a report to the membership.

## Section 4. Quorum.

A quorum of three members or fifty percent, whichever is higher, is required for a valid meeting of the Steering Committee. A Steering Committee member may appoint a proxy who is not already a voting member of the Steering Committee to vote on the member’s behalf when necessary. Unless otherwise specified in the Bylaws, all votes by the Steering Committee require a simple majority of votes cast.

# Article V. Officer Positions and Duties

## Section 1. Definition

The officers of DSA SF will be the two Co-Chairs, Vice Chair, Secretary, Treasurer and all at-large members. The Grievance Officers shall not be considered “Officers” of DSA SF for purposes of these Bylaws. All officers shall perform the duties prescribed by these Bylaws and the standing rules defined throughout the lifetime of DSA SF. No officer may hold more than one officer position at a time. Officers will be elected to either 6-month or 1-year terms; they will serve for the length of their term or until successors are elected.

## Section 2. Diversity Requirement

Three or more elected officers must be either a woman, transgender, non-binary, or any combination of these categories, determined solely by the officers’ own identification. Additionally, three or more elected officers must be Black, Indigenous, and/or People of Color (BIPOC), again determined solely by the officers’ own identification. If fewer than enough candidates stand for election to satisfy either or both of these requirements these requirements will be altered as described in [Section 7](#section-7-candidacy-and-elections).

At least one Co-Chair must be either a woman, transgender, non-binary, or any combination of these categories, determined solely by that Co-Chair’s own identification. Additionally, at least one Co-Chair must be Black, Indigenous, and/or a Person of Color (BIPOC), again determined solely by the officers’ own identification. If fewer than enough candidates stand for election to satisfy either or both of these requirements, these requirements will be altered as described in [Section 7](#section-7-candidacy-and-elections).

## Section 3. Co-Chairs and Vice Chair

The Co-Chairs shall be the chief spokespeople of the organization. They shall work with the rest of the Steering Committee to facilitate all meetings of the organization as a whole, shall have responsibility for the overall management of the organization, and shall interpret the Bylaws, subject to appeal as described in [Article XIV](#article-xiv-general-provisions). The Vice Chair shall act as deputy to the Co-Chairs and succeed to that position in the event that either Co-Chair resigns or otherwise is removed from office.

## Section 4. Secretary

The Secretary shall be responsible for the functioning of the Chapter Coordination Committee and see that the administrative functions of the chapter are properly carried out, including maintaining accurate membership lists, keeping minutes and records of chapter meetings, maintaining communication channels utilized by DSA SF, and notifying the Steering Committee and the membership of all meetings. Part of this job includes attending CCC meetings and reporting back to the Steering Committee.

## Section 5. Treasurer

The Treasurer shall have custody of all funds of the organization and shall be responsible for the financial management of the organization. The Treasurer shall be responsible for presenting regular budgetary updates at Steering Committee meetings, as well as an end-of-year financial statement. The Treasurer shall be responsible for authorizing all expenditures in accordance with the wishes of the Steering Committee (in the case of expenditures over $300, prior approval must be sought from the Steering Committee), and shall organize dues collection, should DSA SF decide to collect dues.

## Section 6. At-large members

Elected Steering Committee members not selected for a Co-Chair, Vice Chair, Secretary, or Treasurer position are considered at-large members of the Steering Committee.

## Section 7. Candidacy and Elections

Candidates for the Steering Committee must be eligible voters of DSA SF and not under the discipline of any self-defined democratic-centralist organization. Every candidate is simply running to be on the Steering Committee, not for any specific position.

Candidates must be nominated by five members eligible voters, not including themselves, by no later than the last regular meeting before the election is to be held.

At the meeting where the election is held, every valid candidate is given equal time to speak. Voters then rank their choices for candidates. Elections for the Steering Committee will be held according to the rules set forth in [Article XII](#article-xii-elections).

Officers are determined as follows:

1.  In total, seven Steering Committee members shall be elected through the process described in [Article XII](#article-xii-elections). Four members will be elected for one-year terms, serving between chapter conventions. Three members will be elected for six-month terms, with a new Steering Committee election held at a mid-year chapter meeting.

2.  The Co-Chairs of the chapter will be determined by vote of the Steering Committee among elected one-year-term officers who wish to serve. If no two members of the Steering Committee who wish to serve as Co-Chair together satisfy either or both of the Co-Chair diversity requirements then each respective requirement that can’t be satisfied shall be waived.

3.  If the officer [diversity requirements of Section 2](#section-2-diversity-requirement) are not met by the election of the seven Steering Committee members, the candidates who would fulfill the requirement but did not receive enough votes to be elected shall be added as additional elected at-large Steering Committee members, based on the next most number of votes received, until the diversity requirement is satisfied or there are no more qualifying candidates.

4.  The Vice Chair is selected from among the one-year-term committee members by vote of the new Steering Committee. The Secretary & Treasurer should be selected from among the committee members. However, if (and only if) no members of the Steering Committee wish to take the roles, then the roles may be appointed from the general membership. Unelected members appointed in this fashion assume the title, duties, and authorities of that role and become non-voting members of the Steering Committee.

## Section 8. Acting Officers

Should an officer leave DSA SF, resign the position, or be removed from office, a special election must be held as soon as feasible to fill the remainder of that officer’s term unless the regular election for that position is already scheduled to be held within 90 days. The Steering Committee may, but is not required to, select an acting officer to hold the position until the election. If the chapter is not in compliance with the diversity requirement of [Section 2](#section-2-diversity-requirement) after the original officer’s departure, any acting officer selected must satisfy the diversity requirement, and any special election may be limited to candidates who would satisfy the diversity requirement at issue.

## Section 9. Removal of an Officer

An Officer may be removed from their position by a majority of votes cast at a Regular or Special meeting called for that purpose. A removal vote is triggered by a majority vote of the Steering Committee to recommend removal of the Officer, or by a petition of 10% of eligible voters or 15 eligible voters, whichever is greater. The membership must be given at least two weeks notice of the upcoming removal vote, which must be held as soon as feasible after the vote is triggered.

# Article VI. Treasury

The treasury must be deposited in a local credit union; corporate banking institutions are forbidden unless no credit union is available. Local expenditures out of the treasury must prefer contracts with union labor. Corporate donations are forbidden to be accepted by the treasury.

# Article VII. Chapter Bodies

## Section 1. Definition and Purpose

In order to more effectively organize to achieve its mission and objectives, DSA SF may establish boards, committees, and working groups, collectively referred to as chapter bodies. Boards may be established for the purpose of organizing and guiding the political direction of the chapter in specific areas (e.g., electoral politics, chapter education). Committees may be established for the purpose of performing specific functions for the chapter (e.g., chapter coordination and administration, communications). Working groups may be established for the purpose of organizing action around political themes or projects (e.g., housing, immigration, feminism) that fall under the purview of DSA SF’s political platform.

## Section 2. Creation

Members who wish to form a chapter body must submit a proposed charter that includes (1) a group name; (2); mission statement and objectives; (3) strategy for achieving the stated objectives; (4) if it is anticipated that the proposed chapter body will seek authorization to expend the chapter’s funds, a short explanation of the anticipated use of those funds and estimate of the amount to be spent.

Proposed charters for boards, but not committees or working groups, must additionally include (5) a leadership structure composed of at least three and no more than five board members to be elected by the chapter at large.

Proposed charters for committees and working groups, but not boards, must additionally include (5) a list of at least five prospective members signed by those members; (6) a statement of whether the proposed body is to be a standing body until the next Annual Convention or a temporary body to expire at a time certain or upon the occurrence of a specified event; (7) whether membership in the body is to be open to all members in good standing or limited; and (8) if membership is to be limited, a description of said limitations.

A proposed chapter body shall be recognized and chartered with the approval of a majority of the votes cast at a Regular or Special Meeting, or the Annual Convention. Charters for proposed chapter bodies must be published on the agenda or announced to the membership at least two weeks prior to the meeting.


## Section 3. Membership and Elections

Membership on a board shall be open only to the members elected by the chapter to serve on the board.

Membership in a committee or working group shall be open to all DSA SF members in good standing unless the body’s charter limits membership, in which case the terms of the charter shall control. 

All chapter bodies shall maintain an official membership list, updated one week in advance of every Regular or Special Meeting, or the Annual Convention.

If a previously established chapter body is being rechartered, the previous leadership will continue until new leadership is elected. If a chapter body is being chartered for the first time, the charter will include two provisional co-chairs or board members who will serve until leadership is elected.

Between charter ratification and the next Regular meeting, committee/working groups – or for Boards and other chapter bodies elected chapterwide, the chapter – will nominate candidates for the leadership positions of the newly ratified chapter bodies. The chapter will hold elections for these positions using the Single Transferable Vote system described in [Article XIII section 3](https://docs.dsasf.org/bylaws.html#section-3-ranked-choice-voting) and [4](https://docs.dsasf.org/bylaws.html#section-4-ballot-count) at the next Regular meeting or Special meeting called for that purpose.

Leadership of chapter bodies will serve for 6 month terms. Committees/working groups will hold a mid-year leadership election, which is then ratified at a chapter Regular meeting or Special meeting called for that purpose. Boards are nominated and elected by the chapter. After the initial 6-month term, each chapter body will be nominated and elected in the same manner as the initial election. In the case of a mid-term vacancy, the committee/working group, – or for Boards and other chapter bodies elected chapterwide, the chapter – will nominate and elect an interim replacement by the next chapter Regular meeting or Special meeting called for that purpose. Chapter body leadership shall make a statement about their leadership and respective chapter body at the ratification meeting to inform the chapter’s membership before it votes. The charters of chapter bodies shall not redefine leadership election cycles, but may opt into chapterwide leadership elections under their own charter.

All board members and co-chairs must be added to current leadership channels and listservs. All board members and co-chairs must be removed from said channels and listservs after their terms are over if they are not re-elected.

In the interest of sharing leadership opportunities and elevating rank and file members, leaders of chapter bodies (including the Steering Committee) cannot hold more than one concurrent leadership position unless no other eligible members wish to run.

## Section 4. Diversity

At least 50% of the leadership of each chapter body should be either a woman, transgender, non-binary, or any combination of these categories, determined solely by that officer’s own identification. Additionally at least 50% of the leadership of each chapter body should be Black, Indigenous, and/or People of Color (BIPOC). If either of these conditions are not met then the officers elected by the next election for any of these offices shall satisfy all of the unmet conditions, unless no such candidates stand for election, in which case the officers shall satisfy one of the unmet conditions, unless no such candidates stand for election, in which case this requirement is waived.

## Section 5. Duties and Responsibilities

The members of a chapter body are responsible for executing their respective body’s strategy in fulfillment of its objectives.

Board members and co-chairs are responsible for organizing their respective body’s members, running meetings, acting as liaisons to the Steering Committee, and serving as the body’s points of contact to the DSA SF membership.

Co-Chairs may appoint a vice chair, who will provide administrative and organizing support to their body, acting as their deputy and stepping in to fill gaps within leadership as needed.

Chapter bodies must keep official records of meeting dates, attendance, and meeting notes, which must be made available to the DSA SF membership.

Boards may recommend endorsements, sponsor events, and develop policies for the specific political areas designated in their charters. Board meetings shall be open to chapter members unless the Board goes into executive session by a two-thirds vote of the Board. Board endorsement recommendations and policies must be ratified by the chapter at the next Regular meeting following the Board’s decision. Boards shall keep a record of all endorsement and policy votes which must be published to the membership. The Steering Committee may overturn a Board event sponsorship by a vote of two-thirds of its members.

## Section 6. Expenditures

Chapter bodies may seek authorization to expend the organization’s funds if necessary to accomplish their objectives. All requests for authorization must be presented to the Treasurer and Steering Committee and must be approved by the Steering Committee. Chapter bodies shall maintain records of all authorized expenditures made using the organization’s funds and shall provide these records to the Treasurer.

## Section 7. Review, Modification, and Dissolution

A chapter body’s activities shall be reviewed by the Steering Committee at the Regular Steering Committee meetings.

Existing chapter bodies may amend their charters with the approval of a majority of the votes cast at a Regular, Special, or General meeting. Proposed amendments to a chapter body’s charter must be published on the agenda or announced to the membership at least 15 days prior to the meeting.

Chapter bodies that are chartered as temporary shall dissolve automatically upon the expiration of the certain time or occurrence of the particular event specified in their charter. All other chapter bodies shall expire automatically at the next Annual Convention following their establishment. The elected leadership of each chapter body will make a report on the body’s activities to deliver at the Annual Convention, including a recommendation for ceasing or continuing their work into the following year. If continuance is recommended, they will submit an updated charter for the chapter to ratify at the Annual Convention. The Steering Committee may also provide its own report on chapter bodies and recommendations on continuance in advance of the Annual Convention.

Upon dissolution, the member(s) maintaining custody of the chapter body’s records shall provide copies of those records to the Secretary.

## Section 8. Provisional Working Groups

Working groups may be created on a provisional basis so the chapter has a means to support nascent organizing work without a vote at a meeting. To form a provisional working group, members interested in forming the provisional working group will inform the Steering Committee of their intent to form a provisional working group, naming two provisional co-chairs and stating their purpose and immediate tasks, via a form. The provisional co-chairs must be active members as defined in these Bylaws. Once the Steering Committee has acknowledged this form submission and verified the eligibility of the provisional co-chairs, the group is considered to be a chartered provisional working group.

After two months, the provisional working group must either submit a proposal to charter as a full working group to be voted on at the next general meeting or re-apply for a charter as a provisional working group, or else the provisional working group dissolves. After six months, a provisional working group may no longer re-apply for a charter. At any time, fifteen members can call a vote at the next general meeting, or special meeting if such action is within the purview of that meeting, on whether the provisional working group should continue to exist, or call a snap election for the provisional co-chairs at a general or special meeting.

They are not eligible to spend chapter funds without Steering Committee approval. Provisional co-chairs are not considered part of chapter leadership, and may not vouch for provisional working group members to grant them voting power.

# Article VIII. Organizing Body and Meetings

## Section 1. Members

The members of the organization (or “the membership”), during a Regular or Special Meeting or at the Annual Convention, shall be the highest body of the organization. Special Meetings are restricted in scope and shall have the authority to deal with only those specific matters for which they may be called. The Steering Committee shall be the highest body of the organization between Regular and Special Meetings and Conventions, and shall be responsible for the administration of the organization and the implementation of policies formulated by members.

## Section 2. Annual Convention

DSA SF shall hold an Annual Convention annually with at least 30 days notice given to all members. The members shall meet to elect officers and to discuss and decide primarily, but not exclusively, the political orientation of the organization and program direction, including chapter priorities for the year.

Chapter Priorities are time-bound, large-scale structural and/or campaign-based initiatives that require major sustained effort and resources. The chapter will give these initiatives priority including but not limited to the following: time set aside at chapter regular meetings, space on the website, money from the budget, and mobilization of new and inactive members.

The purpose of the Chapter Priorities system as set forth in this section is to focus the chapter’s primary effort and resources on a limited number of democratically chosen collective initiatives, without precluding the chapter from working on other initiatives or projects as the need arises. Such non-priority initiatives or projects are not precluded from obtaining chapter resources such as time set aside at chapter regular meetings, space on the website, money from the budget, or mobilization of new and inactive members. In allocating limited resources such as money from the budget or mobilization capacity, the chapter, the Steering Committee, or any other body as the chapter may designate with the responsibility to allocate resources, shall first ensure that Chapter Priorities receive resources consistent with their enacting Priority Resolution(s) before allocating such resources to non-priorities. Resources that by their nature are not strictly limited, such as space on the website, shall be allocated more freely.

Priority Resolutions must contain the following:

1.  A timeline for implementation and execution after the Annual Convention, laying out milestones for at least every quarter of the next 12 months
2.  A description of the plan beyond those 12 months
3.  A budget laying out estimated expenditures for at least every quarter
4.  A leadership structure by which chapter members in good standing can be elected as leaders responsible for the day-to-day work of implementing the priority.

Leadership for Chapter Priorities will be elected at the next chapter Regular Meeting following the meeting at which the Priority Resolutions are passed. The implementing team must provide report backs at chapter Regular Meetings and, at the end of the 12 months, must present a debrief outlining the outcomes and learnings of the project to the chapter membership during the Annual Convention. Campaign leaders will bring major strategic deviations, which exceed the scope or direction laid out in the campaign proposal, to the general membership or, in between Regular meetings, to the Steering Committee.

Chapter Priorities shall be undertaken between their adoption and the following Annual Convention, unless an alternate timeline is approved. The Steering Committee will put out a call for proposals at least two (2) months in advance of the Annual Convention. Priority Resolutions can be proposed for consideration by submitting a formal proposal to the Steering Committee at least one (1) month in advance of the Annual Convention. The Steering Committee will make these proposals available for members to review at least two (2) weeks in advance of the Annual Convention and provide time on the Annual Convention agenda to discuss and vote on proposed Priority Resolutions.

To be considered at the Annual Convention, a Priority Resolution must have 15 signatures from eligible voters. Each eligible Priority Resolution will first go before the membership individually, requiring a majority vote in order to advance to the prioritization process. The prioritization process will be a ranked-choice election to select up to three (3) Chapter Priorities, with votes tallied by the Scottish Single Transferable Vote (STV) method. If three (3) or fewer Priority Resolutions advance to the prioritization process, all of those advanced will be automatically adopted.

Chapter Priorities can be changed, removed, and/or added at a Regular or Special meeting. A specific Chapter Priority can be removed by a majority vote of the chapter membership at a Regular, or Special Meeting, or the Annual Convention. New Priority Resolutions can be proposed for consideration by submitting a formal proposal to the Steering Committee at least one (1) month in advance of a Regular or Special meeting. The Steering Committee will make these proposals available for members to review at least two (2) weeks in advance of the meeting. The Priority Resolution must contain the same specifications required of Priority Resolutions at the Annual Convention. If there are fewer than three (3) Chapter Priorities, the newly proposed Priority Resolution must receive a majority of votes to be adopted. If there are already three (3) priorities, membership can replace a priority by proposing a new Priority Resolution which must receive a majority of votes to be adopted. The membership must then vote by Scottish STV to select two (2) priorities to keep among the existing three (3) priorities. Any priorities adopted this way shall be undertaken between their adoption and the following Annual Convention, unless an alternate timeline is approved.

## Section 3. Special Meetings

By a majority vote of the Steering Committee or a petition of fifteen eligible voters, a Special Meeting can be called, with notice given to all members at least 15 days prior to the meeting. The call to the Special Meeting shall specify the matters to be discussed therein and during the meeting, no other matter may be brought to the floor.

## Section 4. Regular Meetings

DSA SF shall hold regular meetings at a frequency of at least once per quarter, and up to once per month. The agenda for a Regular Meeting shall include reports from the Steering Committee, reports from any working groups with relevant updates, political discussion, budgetary appropriations, and education as well as open time for members to raise items not included on the meeting agenda.

## Section 5. Voting

Every eligible voter of the chapter as defined in Article III section 5 shall have voting rights at Special and Regular Meetings and at the Annual Convention. Members shall have the ability to renew DSA dues at the start of the Annual Convention.

## Section 6. Quorum

A quorum of ten percent of eligible voters or fifteen eligible voters, whichever is greater, shall be required for valid Special and Regular Meetings and the Annual Convention.

# Article IX. Chapter Coordination Committee

## Section 1. Purpose

The Chapter Coordination Committee (CCC) is a standing body existing for the purpose of organizing the administrative functions of the chapter required to maintain the chapter as a functioning, membership-led body within the larger national organization, on behalf of the membership and accountable directly to the Steering Committee through the Secretary. CCC has two co-chairs, elected by the chapter who are responsible for the execution of CCC’s goals and the internal committee structure required to achieve them. Any member of DSA SF can help with CCC projects.

## Section 2. Candidacy and Elections

Candidates for CCC co-chair must be eligible voters in DSA SF.

CCC co-chair terms shall be six months. There will be two regular co-chair elections annually, one held at the next regular meeting following convention, and the other held at the next regular meeting following the mid-year regular meeting in which 6 month Steering Committee seat elections are held. 

## Section 3. CCC Co-Chairs

CCC co-chairs are elected by membership at large and serve on their behalf. The co-chairs and the committee as a whole are accountable directly to the Steering Committee through the Secretary, and charged with ensuring CCC fulfills its purpose to the organization.

## Section 4. CCC Activity

Being active in CCC fulfills the criteria for “active” membership in the chapter.

## Section 5. Removal of an Officer

CCC co-chairs may be removed by any procedure outlined for the removal of chapter officers, committee/working group co-chairs, or other leadership.

# Article X. Endorsements, Event Sponsorships, and Online Voting

## Section 1. Formal Endorsements and Event Sponsorships

Candidates running for local, state, and national political office may be formally endorsed by DSA SF as a whole at a Regular, or Special Meeting or the Annual Convention. The endorsement vote must be published on the agenda or announced to the membership at least two weeks prior to the meeting. A sixty percent majority of the votes cast will be required for DSA SF to endorse any candidate.
In addition to endorsing candidates for office, DSA SF may also choose to formally endorse particular positions on local, state, and national issues of interest to the chapter, or sponsor events, protests, or direct actions so long as the positions, events, protests, or direct actions are relevant to and consistent with the vision, mission, and objectives of DSA SF as set forth in [Article II](#article-ii-purpose). Such positions may be endorsed, or events, protests, or direct actions sponsored, by DSA SF as a whole at a Regular, or Special meeting or at the Annual Convention. The endorsement or sponsorship vote must be published on the agenda or announced to the membership at least two weeks prior to the meeting. A sixty percent majority of the votes cast will be required for DSA SF to endorse any position or sponsor any event, protest, or direct action.
In addition, the Steering Committee has the power to sponsor events, protests, or direct actions (but not to endorse positions or candidates) at any time by a vote of two-thirds of its members. However, it may not vote to sponsor events, protests, or direct actions if doing so would conflict with or be inconsistent with a prior chapter vote, and it must report any such decisions back to the chapter as soon as reasonably possible, but by no later than at the next Regular meeting, or the Annual Convention, whichever is earliest. If the sponsored event, protest, or direct action has not yet taken place at the time of that meeting, any eligible voter may move from the floor to overturn the sponsorship by a simple majority of the votes cast.

## Section 2. Online Voting

The Steering Committee may decide, at the request of any member making such a proposal, to hold an online vote on any issue except for candidate or ballot initiative endorsements. Online votes will be open to eligible voters, as defined in [Article III Section 5](#section-5-eligible-voter), as of the start of the online voting period following close of debate. The minimum number of votes cast must be greater than or equal to the number of eligible members in attendance at whichever meeting of the last two regular chapter meetings had the largest number of eligible members in attendance, otherwise the vote will be considered invalid for lack of a quorum.
The Steering Committee shall announce all online votes to the chapter by email and all other reasonable means. There shall be a 72-hour period for debate following the announcement. At the close of debate, there shall be a 48-hour voting period during which any eligible member may vote. No motions to call the question or extend debate will be allowed, and no amendments will be allowed. However, at any time after the start of debate, any eligible member may motion to table the proposal with a simple majority vote.

All voting thresholds for a passing vote shall be the same as if the vote were held at an in-person meeting. Votes to abstain will count toward the quorum. A vote can become binding before the close of the 48-hour voting period if the number of votes in favor or against has surpassed the threshold that would be required if all eligible votes were cast.

## Section 3. Ban on Misrepresentation of Formal Endorsements

No officer or member of DSA SF shall publicly misrepresent the Chapter’s formal endorsements or positions. Intentional or willful public misrepresentation of the Chapter’s endorsements or positions is grounds for suspension or expulsion under [Article III Section 7](#section-7-suspension-and-expulsion).

# Article XI. National Delegates

## Section 1. Selection of Delegates

Delegates to the National DSA Convention shall be elected by the membership. In accordance with the national Bylaws, this election shall be held no earlier than four months, and no later than forty-five days, prior to the opening of the National Convention. No election for delegates shall be conducted before the apportionment of delegates has been received from the National DSA.

Elections shall be held, with 30 days notice, according to the procedure and rules specified in [Article XII](#article-xii-elections).

# Article XII. Elections

## Section 1. Nominee Campaigns

Campaigns for DSA SF offices shall be limited to the 30 day window between notice of the Regular Meeting and the election meeting itself. All Nominees will have equal opportunity to address the membership at the meeting.

## Section 2. Process

Elections for DSA SF leadership and delegate positions may be held at a Regular, or Special Meeting. All candidates shall be elected at the meeting through secret ballot by  eligible voters. Ballots must be counted immediately following the final ballot being cast. Ballots must be counted twice by a group between two and five active members in good standing in full unobstructed view of any interested Members acting as election observers. All reasonable efforts should be made to find counters that are acceptable to all candidates.

## Section 3. Ranked Choice Voting

All elections in the chapter will use ranked choice voting. "Ranked Choice Voting" means a method of casting and tabulating votes that simulates the ballot counts that would occur if all voters participated in a series of runoff elections with one candidate eliminated after each round of counting. In elections using the Ranked Choice Voting method, voters may rank the candidates in order of preference.

## Section 4. Ballot Count

Votes are tallied by the Scottish Single Transferable Vote (STV) method. Candidates are elected according to this count procedure in the order that they receive the requisite votes.

# Article XIII. Dissolution

In the event of the dissolution of DSA SF, all remaining funds and assets are to be released to the nearest DSA chapter most likely to inherit its members or the DSA National when a nearby chapter cannot be found.

# Article XIV. General Provisions

## Section 1. Interpretation

These Bylaws shall be interpreted by the Co-Chairs, subject to appeal by any member to the full Steering Committee between meetings of the chapter, or during Regular, Special or the Annual Convention, by motion from the floor and chapter vote. All powers not delegated in these Bylaws or by vote of the membership are reserved to the membership.

## Section 2. Rules

The rules contained in the current edition of Robert’s Rules of Order Newly Revised shall govern the organization in all cases to which they are applicable and in which they are not inconsistent with these Bylaws or standing rules of DSA SF, except that the chapter or a committee may vote to use modified or alternative rules of procedure in their meetings so long as they are not inconsistent with these Bylaws.

# Article XV. Amendments

## Section 1. Qualifying Proposals

These Bylaws may be amended only on written proposal of at least fifteen co-sponsors who are eligible voters, or of 10% of eligible voters, whichever is less. Proposals must identify both the proposed language of the amendment and the current language in the Bylaws to be deleted, replaced, or amended.

Proposals must be submitted in writing to the Steering Committee, which as soon as feasible shall circulate all proposals that satisfy the sponsorship and identification requirements of this section to the membership for review.

## Section 2. Introduction and Ratification

The co-sponsors shall introduce their proposals to the chapter at the first Regular Meeting, or Special Meeting called for that purpose, or the Annual Convention, taking place at least two weeks after the proposals are circulated to the membership. The proposals shall not be voted on during the meeting.

No later than one week after the meeting, the proposals’ co-sponsors shall inform the Steering Committee in writing whether they choose to withdraw the proposals, make any changes to the proposals they deem necessary, or reaffirm that they wish to proceed with the proposals as introduced. Unless withdrawn, the Steering Committee shall then recirculate the proposals to the chapter as soon as feasible.

The chapter will vote whether to ratify the proposals at the next Regular Meeting, or Special Meeting called for that purpose, or the Annual Convention, taking place at least two weeks after the proposals are recirculated to the chapter. Ratification requires a two-thirds majority of the votes cast.

# Article XVI. Grievance Officers

## Section 1. Definition and Scope

**Definition.** The Grievance Officers (“GOs”) will be two members elected by the chapter to hear and provide recommendations on grievances against chapter members. The two GOs will be elected simultaneously using the same method as used to elect the Steering Committee. The term of the GOs will be one year or until successors are elected.

**Scope.** The complaints that the GOs are empowered to address will encompass the following:

1.  Complaints regarding harassment on the basis of sex, gender, gender identity or expression, sexual orientation, physical appearance, disability, race, color, religion, national origin, class, age, or profession. Harassing or abusive behavior, such as unwelcome (in the sense that the complainant did not solicit or incite it, and found it undesirable or offensive) attention, inappropriate or offensive remarks, slurs, or jokes, physical or verbal intimidation, stalking, inappropriate physical contact or proximity, and other verbal and physical conduct constitute harassment when:
    1.  Submission to such conduct is made either explicitly or implicitly a term or condition of a member’s continued affiliation with DSA;
    2.  Submission or rejection of such conduct by an individual is used as the basis for organizational decisions affecting such individual; or
    3.  Such conduct has the purpose or effect of creating a hostile environment interfering with an individual’s capacity to organize within DSA. A “hostile environment” is one in which the harassment is sufficiently severe or pervasive as to alter the conditions of membership and create an abusive organizing environment. Whether harassment is severe enough to reach this level is determined by whether a reasonable person in the complainant’s position (“position” being defined broadly to include whether, for example, the complainant is a member of a group that is often marginalized or discriminated against) would be offended by the conduct.
2.  Complaints regarding alleged violations of the DSA SF Code of Conduct other than:
    1.  Those relating to claiming to speak on behalf of or represent the chapter without its approval, and
    2.  Those relating to a candidate’s advertising or publicizing of their member status in a manner designed to promote their candidacy.

## Section 2. Grievance Officer Role and Responsibilities

**Role.** The GOs will follow the process laid out in this section upon receipt of a written complaint submitted to them by a complainant.

**Process.** After a written complaint has been received by the GOs:

1.  The GOs will contact the complainant to acknowledge receipt;
2.  The GOs will contact the accused member within seven days to notify them that a complaint has been filed against them and request a written response to the complaint either affirming or denying its substance to be submitted within seven days of being notified;
3.  The GOs will as soon as possible (and at any time during the grievance process) make a joint recommendation to the Steering Committee on whether immediate action (such as a temporary suspension or an instruction to the accused not to contact the complainant) is necessary to preserve the safety of the complainant and/or any other members, and/or whether the Steering Committee should retain legal or other professional help to assist in the response to the complaint;
4.  Upon receipt of the accused’s response or if the accused fails to respond within seven days, the GO(s) responsible for adjudicating the dispute will determine whether the complaint is credible and, if necessary, make a recommendation to the Steering Committee of appropriate disciplinary or remedial action as soon as practicable, but ultimately within thirty days of the complaint being filed. The GO(s) may notify the Steering Committee of the complaint and its substance at any time after the complaint is filed, but must give written notice to both the complainant and the accused member before doing so.
5.  In determining whether a complaint is “credible,” the following standards shall apply. A factual allegation in a complaint is "credible" if it more-likely-than-not occurred. An allegation in a complaint that particular behavior constituted harassment (for example, if there is a dispute about whether a particular statement or comment had the intent or effect of causing a hostile environment) is "credible" if a reasonable person in the complainant's position would agree that the behavior constituted harassment.
6.  In determining whether the complaint is credible, the GOs overseeing the dispute should, to the extent necessary and possible, investigate the complaint by:
    1.  Interviewing other members with direct knowledge of the substance of the report;
    2.  Requesting documentation from either the accuser or accused or any other parties directly involved; and
    3.  Employing any other legal means, with the utmost respect for the confidentiality of the parties. If the accused refuses to submit a written response within seven days, the GOs may (but are not required to) consider the refusal to respond as evidence that the complaint is credible.
7.  In the case of a complaint made against the Steering Committee or any of its members, the GOs may, at their discretion and subject to the consent of the complainant(s), publish their recommendations for disciplinary or remedial action publicly to chapter members (removing all identifying information of involved parties except the Steering Committee, unless asked not to), and, no more than thirty days later, report publicly on whether the recommendations were adhered to.
8.  In the case of a complaint alleging retaliation against a complainant, which could include threats, intimidation, reprisals and/or adverse actions related to organizing, the GOs will determine whether to consolidate the retaliation into the original complaint, or to treat it as a separate complaint.

### Disciplinary and Remedial Recommendations

In the event that the GOs determine a complaint is not credible, they may recommend either that the Steering Committee take no action on the complaint or that it request the parties undergo voluntary, informal conflict resolution. In the event that the GOs determine that a complaint is credible, they may recommend from among the below disciplinary or remedial actions. The recommended action should be commensurate with the severity and pervasiveness of the offense.

1.  Expulsion from DSA SF;
2.  Temporary or indefinite suspension and/or removal of the accused from some or all DSA SF activities, events, spaces, committees, and communication channels;
3.  Formal notice of expected changes in behavior;
4.  Voluntary, informal mediation or conflict resolution;
5.  No action; OR
6.  Other disciplinary or remedial measures as necessary to protect the complainant and/or other members from an unsafe space or to repair harm done to individuals, relationships and/or the organization.

### Other Responsibilities

The GOs will also:

1.  Archive all complaints, responses of accused members, investigatory materials, and recommendations for at least one year after the closing of the grievance process.
2.  Organize a once-yearly training on harassment and Code of Conduct violations, or ensure another member does so.
3.  Compile a yearly report that details:
    1.  How many complaints were made and how many disciplinary actions were taken in the preceding calendar year; and
    2.  Any recommended changes for making the reporting system more effective. The report will not include personally identifying information of any parties in any dispute.
4.  The local GOs will send the yearly report to the national Harassment GO(s) as soon as possible after January 1 of each year.

### Consensus and Recusal

**Consensus.** When addressing a complaint, GOs must act through consensus whenever possible. When this is not possible, each GO will prepare a proposal for the Steering Committee containing recommendations for action. The Steering Committee may choose to utilize parts of each proposal or to adopt one or the other.

**Recusal.** Any GO who is personally involved in the issues raised in the Grievance must recuse themself. GOs may also voluntarily recuse themselves in other situations where they do not believe they can fairly investigate the grievance.

A complainant or accused party may also request that a GO recuse themself from a case if they feel there will not be a fair investigation, in which case that GO must recuse themself.
In the event that all GOs recuse themselves, the Steering Committee shall appoint a non-conflicted member who is not a Steering Committee member to temporarily take on the GO role to handle the grievance. The Steering Committee may also require the temporary GO to enlist a professional.

## Section 3. Candidacy and Elections

Any eligible voters of the chapter may run to be a GO.
GOs will be elected through the same process as the Steering Committee, including the requirements for nomination and advanced notice for election.
Upon election, each Grievance Officer will receive training.

## Section 4. Vacancies

Should a grievance officer leave DSA SF, resign the position, or be removed from office, the position will be left vacant until an election can be conducted. This must occur within 60 days after the vacancy and can take place at a Regular Meeting, or a Special Meeting called for that purpose, or at the Annual Convention.

## Section 5. Removal of a GO

Grievance officers may be removed by a referendum vote of the Chapter's members, by the same procedure required for the removal of Officers.
